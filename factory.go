package tine

import (
	"context"
)

type Factory struct {
	Payload interface{}
}

func MakeAction(ctx context.Context, p interface{}) (*Factory, ActionError, context.Context) {
	return &Factory{
		Payload: p,
	}, nil, ctx
}
