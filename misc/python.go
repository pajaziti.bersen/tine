package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

func main() {

	r, err := os.Getwd()

	if err != nil {
		log.Println(err)
	}

	if len(os.Args) < 3 {
		log.Fatal("Wrong arguments!")
	}

	_, d, ra := os.Args[0], os.Args[1], os.Args[2:]

	c := strings.Join(ra, " ")

	fp := filepath.Join(r, "example/app/cmd", d)

	b := fmt.Sprintf("%s %s %s", "python3", fp, c)

	f := []byte(b)

	fmt.Println(b)

	err = os.WriteFile("./run.sh", f, 0755)

	if err != nil {
		log.Println(err)
	}

	app := "bash"

	arg0 := "./run.sh"

	cmd := exec.Command(app, arg0)

	o, err := cmd.CombinedOutput()

	os.Remove("./run.sh")

	if err != nil {

		cr := "\033[31m"
		fmt.Println(string(cr), ">", string(o))
		return
	}

	fmt.Println(string(o))

}
