package main

import (
	"context"
	"gitlab.com/pajaziti.bersen/tine/action/cli"
	"log"
)

func main() {

	ctx := context.Background()

	cmd, err := cli.MakeAction(ctx, cli.Payload{
		Cmd:  "python3 @wd/app.py",
		Path: "pkg/sparta",
	})

	if err != nil {
		log.Println(err)
	}

	_, err = cmd.UseCase(ctx)

	if err != nil {
		log.Println(err)
	}

}
