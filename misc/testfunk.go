package main

import (
	"fmt"
	"github.com/thoas/go-funk"
)

func main() {

	type Payload2 struct {
		Requestdata string
		Mapdata     string
		Dump        string
	}

	type Payload struct {
		Requestdata Payload2
		Mapdata     string
		Dump        string
	}

	foo := &Payload{
		Requestdata: Payload2{
			Requestdata: "trip",
			Mapdata:     "meta",
			Dump:        "dump",
		},
		Mapdata: "meta",
		Dump:    "dump",
	}

	a := funk.Get(foo, "Requestdata.Requestdata")

	fmt.Println(a)

}
