package main

import (
	"encoding/json"
	"fmt"
)

func main() {

	e, _ := json.Marshal(map[string]interface{}{
		"apiKey":    "a4db08b7-5729-4ba9-8c08-f2df493465a1",
		"sendMail":  true,
		"maxEmails": 3,
	})

	fmt.Println(string(e))

}
