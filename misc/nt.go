package main

import (
	"context"
	"gitlab.com/pajaziti.bersen/tine/action/cli"
	"log"
)

func main() {

	ctx := context.Background()

	cmd, err := cli.MakeAction(ctx, cmd.Payload{
		Cmd:  "python3 nt -wd @wd",
		Path: "example/__",
	})

	if err != nil {
		log.Println(err)
	}

	_, err = cmd.UseCase(ctx)

	if err != nil {
		log.Println(err)
	}

}
