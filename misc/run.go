package main

import (
	"context"
	"gitlab.com/pajaziti.bersen/tine/action/cli"
	"log"
	"os"
)

func main() {
	ctx := context.Background()

	_, project, args := os.Args[0], os.Args[1], os.Args[2:]

	cmd, err := cli.MakeAction(ctx, cli.Payload{
		Cmd:  "cd @wd && go run .",
		Path: project,
		Args: args,
	})

	if err != nil {
		log.Println(err)
	}

	_, err = cmd.UseCase(ctx)

	if err != nil {
		log.Println(err)
	}
}
