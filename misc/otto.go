package main

import (
	"context"
	"fmt"
	"log"
	"reflect"
	"strings"
	"unsafe"

	"github.com/robertkrimen/otto"
	"gitlab.com/pajaziti.bersen/tine"
	. "gitlab.com/pajaziti.bersen/tine/script"
)

func setAll(ctx interface{}, vm *otto.Otto) {
	contextValues := reflect.ValueOf(ctx).Elem()
	contextKeys := reflect.TypeOf(ctx).Elem()

	if contextKeys.Kind() == reflect.Struct {

		var k string

		for i := 0; i < contextValues.NumField(); i++ {
			reflectValue := contextValues.Field(i)
			reflectValue = reflect.NewAt(reflectValue.Type(), unsafe.Pointer(reflectValue.UnsafeAddr())).Elem()

			reflectField := contextKeys.Field(i)

			if reflectField.Name == "Context" {
				setAll(reflectValue.Interface(), vm)
			} else if reflectField.Name == "key" {
				k = fmt.Sprintf("%v", reflectValue.Interface())
			} else if reflectField.Name == "val" && strings.Contains(k, "tinecontextkey") {
				vm.Set(strings.ReplaceAll(k, "tinecontextkey", ""), reflectValue.Interface())
			}
		}
	}
}

func main() {

	ctx := context.Background()

	ctx, _ = tine.MakeActionContext(ctx, FormatActionName("test"),
		tine.Define("lorem", "13"))

	ctx, _ = tine.MakeActionContext(ctx, FormatActionName("test"),
		tine.Define("action", "jsonpath"))

	vm := otto.New()

	setAll(ctx, vm)

	vm.Set("ctx", ctx)

	value, err := vm.Run("lorem")

	log.Println(value)

	if err != nil {
		log.Println(err)
	}

}
