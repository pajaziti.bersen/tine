package main

import (
	"context"
	"gitlab.com/pajaziti.bersen/tine/action/cli"
	"log"
	"os"
)

func main() {

	ctx := context.Background()

	_, d := os.Args[0], os.Args[1]

	c := "cd @wd && go get -t . && go build ."

	cmd, err := cli.MakeAction(ctx, cli.Payload{
		Cmd:  c,
		Path: d,
	})

	if err != nil {
		log.Println(err)
	}

	_, err = cmd.UseCase(ctx)

	if err != nil {
		log.Println(err)
	}
}
