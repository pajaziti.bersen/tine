package main

import (
	"context"
	"log"
)

func main() {
	ctx := context.Background()

	cmd, err := cmd.MakeAction(ctx, cmd.Payload{
		Cmd:  "python3 @wd/__/cmd/gencode",
		Path: "example",
		Args: []string{"-d", "tel:038606050"},
	})

	if err != nil {
		log.Println(err)
	}

	_, err = cmd.UseCase(ctx)

	if err != nil {
		log.Println(err)
	}

}
