package process

import (
	"context"
	"reflect"
	"strings"

	"gitlab.com/pajaziti.bersen/tine"
)

func (f *Factory) UseCase(ctx context.Context) (interface{}, tine.ActionError) {
	elem := reflect.ValueOf(f.Payload).Elem()

	relType := elem.Type()

	var res interface{}

	for i := 0; i < relType.NumField(); i++ {

		k := strings.ToLower(relType.Field(i).Name)

		a := elem.Field(i).Interface()

		ca, is_create_action := a.(func(ctx context.Context) (tine.Action, tine.ActionError, context.Context))

		if is_create_action {

			var (
				action tine.Action
				err    error
			)

			action, err, ctx = ca(ctx)

			if err != nil {
				return nil, tine.MakeActionError(ctx, err)
			}

			res, err = action.UseCase(ctx)

			if err != nil {
				return nil, tine.MakeActionError(ctx, err)
			}

			ctx = tine.SetVariable(ctx, k, res)

		}

	}

	return res, nil
}
