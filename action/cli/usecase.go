package cli

import (
	"context"
	"fmt"
	"os"
	"os/exec"

	"gitlab.com/pajaziti.bersen/tine"
)

func (f *Factory) UseCase(ctx context.Context) (interface{}, tine.ActionError) {

	cmd := exec.Command(f.Args[0], f.Args[1])

	o, err := cmd.CombinedOutput()

	os.Remove(f.Args[1])

	if err != nil {

		cr := "\033[31m"

		fmt.Println(string(cr), ">", string(o))

		return nil, tine.MakeActionError(ctx, err)
	}

	fmt.Println(string(o))

	return nil, nil

}
