package cli

import (
	"context"

	"gitlab.com/pajaziti.bersen/tine"
	"gitlab.com/pajaziti.bersen/tine/action/shell"
)

type Factory struct {
	Args []string
}

type Payload = shell.Payload

func MakeAction(ctx context.Context, p interface{}) (*Factory, tine.ActionError) {

	a, err := shell.MakeAction(ctx, p)

	if err != nil {
		return nil, tine.MakeActionError(ctx, err)
	}

	res, err := a.UseCase(ctx)

	fn := res.(string)

	return &Factory{
		Args: []string{"sh", fn},
	}, nil

}
