package resolve

import (
	"context"

	"gitlab.com/pajaziti.bersen/tine"
)

type Payload struct {
	Data  []map[string]interface{} `json:"data"`
	Rows  []interface{}            `json:"rows"`
	Field string                   `json:"field"`
}
type Factory struct {
	Payload
}

func MakeAction(ctx context.Context, p interface{}) (*Factory, tine.ActionError, context.Context) {

	var payload Payload

	aerr := tine.CastStruct(ctx, p, &payload)

	if aerr != nil {
		return nil, aerr, ctx
	}

	return &Factory{
		Payload: payload,
	}, nil, ctx
}
