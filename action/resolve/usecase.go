package resolve

import (
	"context"

	"gitlab.com/pajaziti.bersen/tine"
)

func (f *Factory) UseCase(ctx context.Context) (interface{}, tine.ActionError) {

	for i, _ := range f.Payload.Data {
		f.Payload.Data[i][f.Payload.Field] = f.Payload.Rows[i]
	}

	return f.Payload.Data, nil
}
