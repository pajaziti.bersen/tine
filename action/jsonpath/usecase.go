package jsonpath

import (
	"context"
	"reflect"
	"strings"

	"github.com/PaesslerAG/jsonpath"
	"gitlab.com/pajaziti.bersen/tine"
)

func (f *Factory) UseCase(ctx context.Context) (interface{}, tine.ActionError) {

	pth, is_string := f.Paths.(string)

	if is_string {

		result, err := jsonpath.Get(pth, f.Payload)

		if err != nil {
			return nil, tine.MakeActionError(ctx, err)
		}

		return result, nil

	}

	elem := reflect.ValueOf(f.Paths).Elem()

	relType := elem.Type()

	result := make(map[string]interface{})

	for i := 0; i < relType.NumField(); i++ {

		k := strings.ToLower(relType.Field(i).Name)

		pi := elem.Field(i).Interface()

		p, ok := pi.(string)

		if ok {
			r, err := jsonpath.Get(p, f.Payload)

			if err != nil {
				return nil, tine.MakeActionError(ctx, err)
			}

			result[k] = r
		}
	}

	return result, nil
}
