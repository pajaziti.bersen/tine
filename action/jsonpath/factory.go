package jsonpath

import (
	"context"

	"gitlab.com/pajaziti.bersen/tine"
)

type Factory struct {
	Payload interface{}
	Paths   interface{}
}

func MakeAction(ctx context.Context, p interface{}) (*Factory, tine.ActionError, context.Context) {
	paths := tine.Variable(ctx, "transform")

	return &Factory{
		Payload: p,
		Paths:   paths,
	}, nil, ctx
}
