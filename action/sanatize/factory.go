package sanatize

import (
	"context"

	"gitlab.com/pajaziti.bersen/tine"
)

type Factory struct {
	Struct interface{}
}

func MakeAction(ctx context.Context, p interface{}) (*Factory, tine.ActionError, context.Context) {
	return &Factory{
		Struct: p,
	}, nil, ctx
}
