package sanatize

import (
	"context"

	"github.com/go-playground/validator/v10"
	"gitlab.com/pajaziti.bersen/tine"
)

func (f *Factory) UseCase(ctx context.Context) (interface{}, tine.ActionError) {
	validate := validator.New()

	err := validate.Struct(f.Struct)

	if err != nil {
		return nil, tine.MakeActionError(ctx, err)
	}

	return f.Struct, nil
}
