package sanatize

import (
	"context"

	"github.com/go-playground/validator/v10"
	"gitlab.com/pajaziti.bersen/tine"
)

func (f *Factory) Validate(ctx context.Context) tine.ActionError {
	validate := validator.New()

	err := validate.Struct(f.Struct)

	if err != nil {
		return tine.MakeActionError(ctx, err)
	}

	return nil
}
