package set

import (
	"context"

	"gitlab.com/pajaziti.bersen/tine"
)

type Payload struct {
	Input interface{} `json:"input"`
	Value interface{} `json:"value"`
	Path  string      `json:"path"`
}
type Factory struct {
	Payload
}

func MakeAction(ctx context.Context, args interface{}) (*Factory, tine.ActionError, context.Context) {

	pargs := args.([]interface{})

	return &Factory{
		Payload: Payload{
			Input: pargs[0],
			Value: pargs[1],
			Path:  pargs[2].(string),
		},
	}, nil, ctx
}
