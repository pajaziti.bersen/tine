package set

import (
	"context"

	"github.com/thoas/go-funk"
	"gitlab.com/pajaziti.bersen/tine"
)

func (f *Factory) UseCase(ctx context.Context) (interface{}, tine.ActionError) {

	funk.Set(f.Payload.Input, f.Payload.Value, f.Payload.Path)

	return f.Payload.Input, nil
}
