package geturls

import (
	"context"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"gitlab.com/pajaziti.bersen/tine"
)

func (f *Factory) UseCase(ctx context.Context) (interface{}, tine.ActionError) {
	s := session.New(f.Config)

	s3s := s3.New(s)

	urls := []string{}

	for i, key := range f.Payload.Keys {

		bucket := f.Payload.Buckets[i]

		req, _ := s3s.GetObjectRequest(&s3.GetObjectInput{
			Bucket: aws.String(bucket),
			Key:    aws.String(key),
		})

		u, err := req.Presign(90 * time.Minute)

		if err != nil {
			return nil, tine.MakeActionError(ctx, err)
		}

		urls = append(urls, u)

	}

	return urls, nil
}
