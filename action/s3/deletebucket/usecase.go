package deletebucket

import (
	"context"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"gitlab.com/pajaziti.bersen/tine"
)

func (f *Factory) UseCase(ctx context.Context) (interface{}, tine.ActionError) {
	s := session.New(f.Config)

	s3s := s3.New(s)

	o, err := s3s.DeleteBucket(&s3.DeleteBucketInput{
		Bucket: aws.String(f.Bucket),
	})

	if err != nil {
		return nil, tine.MakeActionError(ctx, err)
	}

	return o, nil
}
