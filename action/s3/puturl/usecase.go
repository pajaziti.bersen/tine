package puturl

import (
	"context"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"gitlab.com/pajaziti.bersen/tine"
)

func (f *Factory) UseCase(ctx context.Context) (interface{}, tine.ActionError) {
	s := session.New(f.Config)

	s3s := s3.New(s)

	req, _ := s3s.PutObjectRequest(&s3.PutObjectInput{
		Bucket: aws.String(f.Payload.Bucket),
		Key:    aws.String(f.Payload.Key),
	})

	str, err := req.Presign(90 * time.Minute)

	if err != nil {
		return nil, tine.MakeActionError(ctx, err)
	}

	return str, nil
}
