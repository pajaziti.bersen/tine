package puturl

import (
	"context"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"gitlab.com/pajaziti.bersen/tine"
	_ "gitlab.com/pajaziti.bersen/tine/script"
)

type Factory struct {
	Config  *aws.Config
	Payload Payload
}

type MinioConfig struct {
	AccessKey  string `json:"access_key"`
	SecretKey  string `json:"secret_key"`
	Host       string `json:"host"`
	DisableSSL bool   `json:"disable_ssl"`
	Region     string `json:"region"`
}

type Payload struct {
	Bucket string `json:"bucket"`
	Key    string `json:"key"`
}

func MakeAction(ctx context.Context, p interface{}) (*Factory, tine.ActionError, context.Context) {

	var (
		config  MinioConfig
		payload Payload
	)

	aerr := tine.StructVariable(ctx, "s3", &config)

	if aerr != nil {
		return nil, aerr, ctx
	}

	aerr = tine.CastStruct(ctx, p, &payload)

	if aerr != nil {
		return nil, aerr, ctx
	}

	c := &aws.Config{
		Credentials:      credentials.NewStaticCredentials(config.AccessKey, config.SecretKey, ""),
		Endpoint:         aws.String(config.Host),
		Region:           aws.String(config.Region),
		DisableSSL:       aws.Bool(config.DisableSSL),
		S3ForcePathStyle: aws.Bool(true),
	}

	return &Factory{
		Config:  c,
		Payload: payload,
	}, nil, ctx
}
