package request

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/pajaziti.bersen/tine"
)

func (f *Factory) UseCase(ctx context.Context) (interface{}, tine.ActionError) {
	resp, err := http.Get(f.Url)

	if err != nil {
		return nil, tine.MakeActionError(ctx, err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return nil, tine.MakeActionError(ctx, err)
	}

	r := map[string]interface{}{}

	json.Unmarshal(body, &r)

	return r, nil

}
