package request

import (
	"context"
	"encoding/json"

	"gitlab.com/pajaziti.bersen/tine"
)

type Payload struct {
	Url string
}

type Factory struct {
	Url string
}

func MakeAction(ctx context.Context, u interface{}) (*Factory, tine.ActionError, context.Context) {

	var p Payload

	b, err := json.Marshal(u)

	if err != nil {
		return nil, tine.MakeActionError(ctx, err), ctx
	}

	err = json.Unmarshal(b, &p)

	if err != nil {
		return nil, tine.MakeActionError(ctx, err), ctx
	}

	return &Factory{
		Url: p.Url,
	}, nil, ctx
}
