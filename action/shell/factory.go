package shell

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/pajaziti.bersen/tine"
)

type Payload struct {
	Cmd  string
	Path string
	Args []string
}

type Factory struct {
	Bashscript string
}

func MakeAction(ctx context.Context, ip interface{}) (*Factory, tine.ActionError) {

	p, _ := ip.(Payload)

	w, err := os.Getwd()

	if err != nil {
		return nil, tine.MakeActionError(ctx, err)
	}

	wd := filepath.Join(w, p.Path)

	cmd := strings.Replace(p.Cmd, "@wd", wd, -1)

	a := strings.Join(p.Args, " ")

	bs := fmt.Sprintf("%s %s", cmd, a)

	return &Factory{
		Bashscript: bs,
	}, nil
}
