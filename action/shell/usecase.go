package shell

import (
	"context"
	"os"

	"gitlab.com/pajaziti.bersen/tine"
)

func (f *Factory) UseCase(ctx context.Context) (interface{}, tine.ActionError) {

	file, err := os.CreateTemp("", "sample")

	_, err = file.Write([]byte(f.Bashscript))

	if err != nil {
		return nil, tine.MakeActionError(ctx, err)
	}

	return file.Name(), nil

}
