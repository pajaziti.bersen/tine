package shell

import (
	"context"
	"testing"
)

func TestRunUseCase(t *testing.T) {
	ctx := context.Background()

	action, err := MakeAction(ctx, Payload{
		Cmd:  "go run @wd",
		Path: "/test.go",
		Args: []string{"s"},
	})

	if err != nil {
		t.Errorf("Failed! got error %s", err)
	}

	res, err := action.UseCase(ctx)

	data := res.(string)

	if err != nil {
		t.Errorf("Failed! got error %s", err)
	}

	t.Logf("Success!")
	t.Logf(string(data))

}
