package yaml

import (
	"context"

	"gitlab.com/pajaziti.bersen/tine"
	"gopkg.in/yaml.v3"
)

func (f *Factory) UseCase(ctx context.Context) (interface{}, tine.ActionError) {
	result, err := yaml.Marshal(f.Payload)

	if err != nil {
		return nil, tine.MakeActionError(ctx, err)
	}

	return result, nil
}
