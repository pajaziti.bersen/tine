package yaml

import (
	"context"
	"testing"
)

type testPayload struct {
	ApiKey string `yaml:"api_key"`
	SendMail bool `yaml:"send_mail"`
	MaxEmails int `yaml:"max_emails"`
}

func TestYamlUseCaseContext(t *testing.T) {
	ctx := context.Background()
	expect := "api_key: a4db08b7-5729-4ba9-8c08-f2df493465a1\nsend_mail: true\nmax_emails: 3\n"

	action, err := MakeAction(ctx, &testPayload{
		ApiKey: "a4db08b7-5729-4ba9-8c08-f2df493465a1",
		SendMail: true,
		MaxEmails: 3,
	})
	
	if err != nil {
		t.Errorf("Failed! got error %s", err)
	}

	result, err  := action.UseCase(ctx)

	data := result.([]byte)

	if err != nil {
		t.Errorf("Failed! got error %s", err)
	}
	
	if string(data) != expect {
		t.Errorf("Failed!\nexpecting:\n\n%s\ngot:\n\n%s\n", expect, string(data))
	}
	
	t.Logf("Success!")
}
