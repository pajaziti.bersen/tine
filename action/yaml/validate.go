package yaml

import (
	"context"

	"gitlab.com/pajaziti.bersen/tine"
	"gopkg.in/yaml.v3"
)

func (f *Factory) Validate(ctx context.Context) tine.ActionError {
	_, err := yaml.Marshal(f.Payload)

	if err != nil {
		return tine.MakeActionError(ctx, err)
	}

	return nil
}
