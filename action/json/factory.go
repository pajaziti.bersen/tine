package json

import (
	"context"

	"gitlab.com/pajaziti.bersen/tine"
)

type Factory struct {
	Payload interface{}
}

func MakeAction(ctx context.Context, p interface{}) (*Factory, tine.ActionError, context.Context) {
	return &Factory{
		Payload: p,
	}, nil, ctx
}
