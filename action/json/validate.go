package json

import (
	"context"

	"encoding/json"
	"gitlab.com/pajaziti.bersen/tine"
)

func (f *Factory) Validate(ctx context.Context) tine.ActionError {
	_, err := json.Marshal(f.Payload)

	if err != nil {
		return tine.MakeActionError(ctx, err)
	}

	return nil
}
