package json

import (
	"context"

	"encoding/json"
	"gitlab.com/pajaziti.bersen/tine"
)

func (f *Factory) UseCase(ctx context.Context) (interface{}, tine.ActionError) {
	result, err := json.Marshal(f.Payload)

	if err != nil {
		return nil, tine.MakeActionError(ctx, err)
	}

	return result, nil
}
