module gitlab.com/pajaziti.bersen/tine

go 1.18

require (
	github.com/PaesslerAG/jsonpath v0.1.1
	github.com/go-playground/validator/v10 v10.11.0
	github.com/robertkrimen/otto v0.0.0-20211024170158-b87d35c0b86f
	github.com/shengdoushi/base58 v1.0.0
	github.com/thoas/go-funk v0.9.2
	github.com/tyler-smith/go-bip32 v1.0.0
	github.com/tyler-smith/go-bip39 v1.1.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/FactomProject/basen v0.0.0-20150613233007-fe3947df716e // indirect
	github.com/FactomProject/btcutilecc v0.0.0-20130527213604-d3a63a5752ec // indirect
	github.com/PaesslerAG/gval v1.0.0 // indirect
	github.com/aws/aws-sdk-go v1.44.70 // indirect
	github.com/dlclark/regexp2 v1.7.0 // indirect
	github.com/dop251/goja v0.0.0-20220806120448-1444e6b94559 // indirect
	github.com/go-playground/locales v0.14.0 // indirect
	github.com/go-playground/universal-translator v0.18.0 // indirect
	github.com/go-sourcemap/sourcemap v2.1.3+incompatible // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/mashingan/smapping v0.1.17 // indirect
	github.com/stretchr/testify v1.7.1 // indirect
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3 // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/sourcemap.v1 v1.0.5 // indirect
)
