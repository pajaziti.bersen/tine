import os
import socketio
import json
import uuid

from aiohttp import web

sio = socketio.AsyncServer(async_mode='aiohttp')
app = web.Application()
sio.attach(app)

file = os.path.dirname(os.path.abspath(__file__)) + '/app.html'

async def index(request):
    with open(file) as f:
        return web.Response(text=f.read(), content_type='text/html')

@sio.event
async def req(sid, message):
    await sio.emit('res', { 'type': 'sent', 'key': str(uuid.uuid4()), 'message': message['input'], 'trigger': 'triggerevent' }, room=sid)
        
    await sio.emit('res', { 'type': 'recieved', 'key': str(uuid.uuid4()), 'message': message['input'], 'trigger': 'triggerevent' }, room=sid)

@sio.event
async def disconnect_request(sid):
    await sio.disconnect(sid)

@sio.event
def disconnect(sid):
    print('Client disconnected')

print()

app.router.add_get('/', index)


async def init_app():
    return app

if __name__ == '__main__':
    web.run_app(init_app())
