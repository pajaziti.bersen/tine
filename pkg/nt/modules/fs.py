import sys
import os
import yaml
import glob

from consts import TRASCRIPTION_KEY

index = sys.argv.index("-f")
file_path = sys.argv[index + 1]

def get_path_type(path):
    if os.path.isdir(path):
        return "dir"
        
    return "file"


with open(file_path, 'r') as f:
    struct = yaml.safe_load(f)
    old_struct = struct.copy()
    folder = old_struct['__']['folder']
    glob_expression = os.path.join(folder, struct['__']['glob'])

    mached = glob.glob(glob_expression, recursive=True)
    new_struct = { "__" : old_struct["__"] }

    for filepath in mached:
        filename = filepath.rsplit(folder, -1)[1]

        if filename not in old_struct:
            file_type = get_path_type(filename)
            new_struct[filename] = { "__": { "type": file_type } }
        else:
            new_struct[filename] = old_struct[filename]

    output = open(old_struct['__']['original_file_path'], 'w+')
    output.write(yaml.dump(new_struct))
    output.close()
