import os
import yaml
import uuid
import tempfile
import sys

from modules.consts import NT_KEY, TRASCRIPTION_KEY

def run_nt(file_path, scope):
    with open(file_path,'r') as f:
      struct = yaml.safe_load(f)
      struct[NT_KEY]['original_file_path'] = file_path
      struct[NT_KEY]['folder'] = scope['folder']
      
      temp_dir = tempfile.mkdtemp()
      temp_file_path = "%s/%s.yaml" % (temp_dir, uuid.uuid4())
    
      output = open(temp_file_path, 'w+')
      output.write(yaml.dump(struct))
      output.close()
 
      module = struct[NT_KEY][TRASCRIPTION_KEY]
      
      mods = os.path.dirname(os.path.realpath(__file__))
      
      if os.path.isfile("%s/modules/%s.py" % (mods, module)):
          os.popen("python3 %s/modules/%s.py -f %s" % (mods, module, temp_file_path))
          print("NT run successfuly %s" % file_path)
      else:
          print("module doesn't exist")
            
            
if __name__ == "__main__":
    wd = sys.argv[sys.argv.index("-wd") + 1]
    
    for(folder, subs, files) in os.walk(wd):
        for f in files:
          
            print(f)
          
            f = "%s/%s" % (wd, f)
            if f.endswith(".nt.tine"):
                run_nt(f, { "folder": "%s/" % folder })


