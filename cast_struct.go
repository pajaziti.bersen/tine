package tine

import (
	"context"
	"encoding/json"
)

func CastStruct(ctx context.Context, i interface{}, s interface{}) ActionError {

	b, err := json.Marshal(i)

	if err != nil {
		return MakeActionError(ctx, err)
	}

	err = json.Unmarshal(b, &s)

	if err != nil {
		return MakeActionError(ctx, err)
	}

	return nil

	// m := smapping.MapTags(i, "json")

	// fmt.Println(m)

	// err := smapping.FillStructByTags(&s, m, "json")

	// if err != nil {
	// 	return MakeActionError(ctx, err)
	// }

	// return nil
}
