install:

```
go install cmd/tine.go
```

build:

```
cd examples/example

tine ./example.tine
```

run:

```
./example -p holydays
```

run in vercel:

```
yarn 

yarn dev
```

