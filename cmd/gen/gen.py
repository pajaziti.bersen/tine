# TODO: WRITE IT IN TINE
import re
import os
import yaml
import subprocess
import argparse
from functools import reduce
from mako.template import Template
from pathlib import Path

file_path = os.path.dirname(os.path.abspath(__file__))


def rm_tree(pth):
    pth = Path(pth)

    if pth.is_file():
        for child in pth.glob('*'):
            if child.is_file():
                child.unlink()
            else:
                rm_tree(child)
        pth.rmdir()


def convert_case(st):
    return st.replace("_", "@").title().replace("@", "")


def get_import_path(ctx):
    return ctx['project']+'/app' + ctx['path'].split(ctx['project']+'/app')[1]


def gen_def(val):
    if isinstance(val, bool):
        return 'bool'

    if isinstance(val, int):
        return 'int64'

    if re.search("\\$\\{`", str(val)):
        if re.search("\\(`", str(val)):
            value = re.findall(r"\(`(.+)`\) ", str(val), re.MULTILINE)

            if re.search("`", value[0]):
                tags = re.findall(r"`(.+)`", value[0], re.MULTILINE)

                return str(value[0]).replace('`'+tags[0]+'`', '')

            return str(value[0])

        return "interface{}"

    if re.search("\\(`", str(val)):
        value = re.findall(r"\(`(.+)`\)", str(val), re.MULTILINE)

        if re.search("`", value[0]):
            tags = re.findall(r"`(.+)`", value[0], re.MULTILINE)

            return str(value[0]).replace('`'+tags[0]+'`', '')

        return str(value[0])

    if isinstance(val, str):
        return 'string'

    return str(type(val))


def gen_value(rval, var='Body'):
    if isinstance(rval, bool):
        return 'true' if rval else 'false'

    if isinstance(rval, int):
        return str(rval)

    if re.search("\\$\\{`", str(rval)):
        if re.search("\\*", str(rval)):
            return var

        return "*"+var

    if re.search("\\(`", str(rval)):
        value = re.findall(r"`(.+)`", str(rval), re.MULTILINE)
        fval = re.sub(r"\(`(.+)`\) ", "", str(rval))

        if re.search("\\*", value[0]):
            rt = re.findall(r"`(.+)`", str(rval), re.MULTILINE)
            t = rt[0].replace("*", "")

            if re.search(' ', t):
                t = t.split(' ')[0]

            if re.search("string", value[0]):
                return convert_case('optional_' + t) + '("' + fval + '")'

            return convert_case('optional_' + t) + '(' + fval + ')'

        if re.search("string", value[0]):
            return '"' + fval + '"'

        return fval

    return '"' + str(rval) + '"'


def gen_tags(value, key):

    if re.search("\\(`", str(value)):
        val = re.findall(r"`(.+)`\)", str(value), re.MULTILINE)

        if re.search("`", val[0]):
            tags = re.findall(r"`(.+)`", val[0], re.MULTILINE)

            return '`json:"' + key + '" ' + tags[0] + '`'

    return '`json:"' + key + '"`'


def gen_content(d, indent=0):
    o = ''

    for key, value in d.items():
        if isinstance(value, dict):
            o += gen_content(value, indent + 1)
        else:
            o += '\t' * (indent) + convert_case(str(key)) + '  ' + \
                gen_def(value) + '  ' + gen_tags(value, key) + "\n"

    return o


def gen_ctx_structures(ctx, indent=0):
    o = []

    for key, value in ctx.items():
        if isinstance(value, dict):
            o.append('type '+convert_case(key) +
                     ' struct {\n' + gen_content(value, indent) + '}\n')

    return ''.join(o)


def gen_structures(params, ctx, indent=0):

    if "." in params:
        return gen_ctx_structures(ctx, indent)

    return gen_ctx_structures(ctx, indent) + 'type Payload struct {\n' + gen_content(params, indent) + '}\n'


def gen_values(d, indent=0):
    o = []

    for key, value in d.items():
        if isinstance(value, dict):
            o.append(gen_values(value, indent + 1))
        else:
            o.append('\t' * (indent) + convert_case(str(key)) +
                     ':  ' + gen_value(value, convert_case(str(key))) + ',')

    return '\n'.join(o)


def format_nested(payload, action):
    def format(a, b):
        key, value = b

        if isinstance(value, dict) and value['__'] and value['__']['action']:
            if action == 'process':
                a[key] = '(`func(ctx context.Context) (tine.Action, tine.ActionError, context.Context) `yaml:"' + \
                    key+'"``) ' + convert_case(key)
            else:
                a[key] = '(`tine.Action `yaml:"'+key+'"``) ' + \
                    convert_case(key)
        else:
            a[key] = value

        return a

    return reduce(format, payload.items(), {})


def get_imports(project_base, payload):
    o = []

    for key, value in payload.items():
        if isinstance(value, dict) and value['__'] and value['__']['action']:
            o.append(project_base + '/' + key)

    return o


def gen_action_instance(payload):
    o = []

    for key, value in payload.items():
        if isinstance(value, dict) and value['__'] and value['__']['action']:
            o.append([convert_case(key), key])

    return o


def get_template(action):
    if action == 'process':
        return Template(filename=file_path + '/templates/process.txt')

    return Template(filename=file_path + '/templates/action.txt')


def gen_context_values(name, val):
    if isinstance(val, dict):
        o = []

        for key, value in val.items():
            o.append(convert_case(key) + ': ' + gen_value(value, key))

        return '&'+convert_case(name)+'{'+', '.join(o) + '}'

    return gen_value(val, name)


def gen_context(ctx, name):
    o = []

    filter_keys = ["name", "project", "root", "import"]

    for key, value in ctx.items():
        if name == 'app' or key not in filter_keys:
            o.append('tine.Define("' + key + '", ' +
                     gen_context_values(key, value) + ')')

    return ',\n\t\t' + ',\n\t\t'.join(o)


def get_scripts(payload):
    o = []

    for key, value in payload.items():
        if re.search("\\$\\{`", str(value)):
            v = re.findall(r"\$\{`(.+)`\}", str(value), re.MULTILINE)
            val = gen_def(value)

            o.append(['Body' if key == '.' else convert_case(
                key), str(v[0]).replace('"', '\"'), val.replace('*', '').replace(' ', '')])

    return o


def get_ctx_scripts(ctx):
    o = []

    for key, value in ctx.items():
        if re.search("\\$\\{`", str(value)):
            v = re.findall(r"\$\{`(.+)`\}", str(value), re.MULTILINE)
            val = gen_def(value)

            o.append(['Body' if key == '.' else
                      key, str(v[0]).replace('"', '\"'), val.replace('*', '').replace(' ', '')])

    return o


def format_imp(imp):
    o = []

    for i in imp:
        a = i.split(" ")

        if len(a) == 2:
            o.append(a[0] + " " + '"' + a[1] + '"')
        else:
            o.append('"' + i + '"')

    return o


def gen_action(ctx, payload, aname=''):
    name = aname if aname else ctx['name']
    action = ctx['action']
    path = ctx['path']
    ctximports = ctx['import'] if 'import' in ctx else []

    localimports = get_imports(get_import_path(ctx), payload)

    if action != 'tine' and action != 'script':
        if(len(action.split("$")) > 1):
            localimports.append(ctx['project'] + action.split('$')[1])
        else:
            localimports.append(
                "gitlab.com/pajaziti.bersen/tine/action/" + action)

    action_instances = gen_action_instance(payload)

    formatted_payload = format_nested(payload, action)

    action = action.split("/")[-1]

    imports = ["context", "gitlab.com/pajaziti.bersen/tine",
               *localimports, *ctximports]

    ctxdefines = gen_context(ctx, name)

    stru = gen_structures(formatted_payload, ctx, 1)

    ccripts = get_ctx_scripts(ctx)

    if "value" in ctx:
        tmp = Template(filename=file_path + '/templates/action_value.txt')

        return tmp.render(
            name=name,
            action=action,
            stru=stru,
            ccripts=ccripts,
            imprts=format_imp(imports),
            ctx=ctxdefines,
        )

    scripts = get_scripts(formatted_payload)

    if "." in payload and action == 'script':
        tmp = Template(filename=file_path + '/templates/action_script.txt')

        return tmp.render(
            name=name,
            action=action,
            action_instances=action_instances,
            stru=stru,
            scripts=scripts,
            ccripts=ccripts,
            imprts=format_imp(imports),
            value=payload["."],
            ctx=ctxdefines,
        )

    if "." in payload:
        tmp = Template(filename=file_path + '/templates/action_simple.txt')

        return tmp.render(
            name=name,
            action=action,
            action_instances=action_instances,
            stru=stru,
            scripts=scripts,
            ccripts=ccripts,
            imprts=format_imp(imports),
            value=gen_value(payload["."]),
            ctx=ctxdefines,
        )

    tmp = get_template(action)

    vals = gen_values(formatted_payload, 2)

    return tmp.render(
        name=name,
        action=action,
        action_instances=action_instances,
        imprts=format_imp(imports),
        scripts=scripts,
        ccripts=ccripts,
        stru=stru,
        vals=vals,
        path=path,
        ctx=ctxdefines,
    )


def write_file(path, data):
    os.makedirs(os.path.dirname(path), exist_ok=True)

    f = open(path, "w")
    f.write(data)
    f.close()


def gen_nested(ctx, payload, path):
    for key, value in payload.items():
        if isinstance(value, dict) and value['__'] and value['__']['action']:
            sctx = value.pop('__')
            sctx['name'] = key
            sctx['path'] = path + '/' + key
            sctx['project'] = ctx['name']

            pth = path + '/' + key + "/" + key + ".tine.go"

            print('########################### ' + pth)
            write_file(pth, gen_action(sctx, value))

        if isinstance(value, dict):
            gen_nested(ctx, value, path + '/' + key)


def gen_cmd(ctx):
    mytemplate = Template(filename=file_path + '/templates/cmd.txt')

    write_file(ctx['path'] + "/../cmd/main.go", mytemplate.render(
        app=get_import_path(ctx)
    ))


def gen_mod(ctx):
    mytemplate = Template(filename=file_path + '/templates/mod.txt')

    reqs = ctx['requires'] if 'requires' in ctx else [
        'gitlab.com/pajaziti.bersen/tine latest']

    reps = ctx['replace'] if 'replace' in ctx else ['']

    write_file(ctx['root'] + "/go.mod", mytemplate.render(
        app=ctx['name'],
        reqs=reqs,
        reps=reps
    ))


def gen_package(ctx):
    mytemplate = Template(filename=file_path + '/templates/package.txt')

    write_file(ctx['root'] + "/package.json",
               mytemplate.render(name=ctx['project']))


def gen_vercel_config(ctx):
    mytemplate = Template(filename=file_path + '/templates/vercel.txt')

    write_file(ctx['root'] + "/vercel.json", mytemplate.render())


def get_api_routes(ctx, payload):
    stru = gen_ctx_structures(ctx, 1)
    ctxdefines = gen_context(ctx, "app")

    for key, value in payload.items():
        if isinstance(value, dict) and value['__'] and value['__']['action']:
            mytemplate = Template(filename=file_path +
                                  '/templates/handler.txt')

            if value['__']['action'] != 'script':

                write_file(ctx['root'] + "/api/" + key + "/index.go", mytemplate.render(
                    func_name=convert_case(key),
                    action=key,
                    app=ctx['project'],
                    stru=stru,
                    ctx=ctxdefines
                ))


def gen_file(ctx, payload):
    print('########################### app')

    rm_tree(ctx['path'])

    write_file(ctx['path'] + "/main.go", gen_action(ctx, payload, 'app'))

    gen_cmd(ctx)
    gen_mod(ctx)
    gen_package(ctx)
    gen_vercel_config(ctx)
    get_api_routes(ctx, payload)
    gen_nested(ctx, payload, ctx['path'])


parser = argparse.ArgumentParser()
parser.add_argument("-f", type=str)

args = parser.parse_args()

with open(args.f, 'r') as file:
    params = yaml.safe_load(file)

if params['__']['use']:
    for f in params['__']['use']:
        with open(f, 'r') as file:
            params_use = yaml.safe_load(file)
            params = dict(list(params.items()) + list(params_use.items()))

ctx = params.pop('__')
ctx['name'] = args.f.split('/')[-1].split('.')[0]
ctx['path'] = '/'.join(os.path.abspath(args.f).split('.')
                       [0].split('/')[:-1]) + '/app'
ctx['root'] = '/'.join(os.path.abspath(args.f).split('.')[0].split('/')[:-1])
ctx['project'] = args.f.split('/')[-1].split('.')[0]


gen_file(ctx, params)
