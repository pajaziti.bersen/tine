package main

import (
	"context"
	"embed"
	"fmt"
	"io/fs"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"gitlab.com/pajaziti.bersen/tine/action/cli"
)

//go:embed gen
var gen embed.FS

func main() {

	wd, err := os.Getwd()

	if err != nil {
		log.Println(err)
	}

	dirName, err := ioutil.TempDir("/tmp", "python")

	if err != nil {
		log.Fatal(err)
	}

	err = fs.WalkDir(gen, ".", func(path string, d fs.DirEntry, err error) error {
		// cannot happen
		if err != nil {
			panic(err)
		}

		filename := fmt.Sprintf("%s/%s", dirName, path)

		if d.IsDir() {

			if d.Name() == "." {
				return nil
			}

			if err := os.Mkdir(filename, 0777); err != nil {
				log.Println(err)
			}

			return nil
		}

		b, err := fs.ReadFile(gen, path)

		if err != nil {
			return err
		}

		if err := os.WriteFile(filename, b, 0777); err != nil {
			log.Printf("error os.WriteFile error: %v", err)
		}

		return nil
	})

	if err != nil {
		log.Println(err)
	}

	script := filepath.Join(dirName, "gen/gen.py")

	filename := filepath.Join(wd, os.Args[1])

	pycmd := exec.Command("python3", script, "-f", filename)

	o, err := pycmd.CombinedOutput()

	if err != nil {
		log.Println(err)
	}

	fmt.Println(string(o))

	ctx := context.Background()

	folder := strings.Replace(filename, ".tine", "", 1)

	a := strings.Split(folder, "/")

	a = a[:len(a)-1]

	folder = strings.Join(a, "/")

	c := fmt.Sprintf("cd %s/cmd && go get -t . && go build .", folder)

	cmd, err := cli.MakeAction(ctx, cli.Payload{
		Cmd: c,
	})

	if err != nil {
		log.Println(err)
	}

	_, err = cmd.UseCase(ctx)

	if err != nil {
		log.Println(err)
	}

}
