package main

import (
	"encoding/json"
	"context"
	"flag"
	"fmt"
	"log"

	"gitlab.com/pajaziti.bersen/tine"
	
	"agorussimple/app"
)

func main() {
	i := flag.String("i", "", "input")

	p := flag.String("p", "", "path")

	flag.Parse()

	var input map[string]interface{}

	if i != nil {
		json.Unmarshal([]byte(*i), &input)
	}

	ctx, err := tine.MakeActionContext(context.Background(), "main", tine.Define("input", input))

	if err != nil {
		log.Fatal(err)
	}

	App, err := app.MakeAction(ctx, "app")

	if err != nil {
		log.Fatal(err)
	}

	a, _ := App.UseCase(ctx)

	actions, ok := a.(map[string]interface{})

	if !ok {
		log.Fatal("Incorrect Format!")
	}

	if *p == "" {
		log.Fatal("Select path!")
	}

	ai, ok := actions[*p]

	if !ok {
		log.Fatal("Wrong path!")
	}

	action, ok := ai.(tine.Action)

	if !ok {
		log.Fatal("Wrong format!")
	}

	r, err := action.UseCase(ctx)

	if err != nil {
		log.Fatal(err)
	}

	data := r.([]byte)

	fmt.Print(string(data))

}
