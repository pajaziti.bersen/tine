module agorussimple

go 1.18

require (
	github.com/mattn/go-sqlite3 v1.14.14
	gitlab.com/pajaziti.bersen/agorus-ent v0.0.0-00010101000000-000000000000
	gitlab.com/pajaziti.bersen/tine v0.0.0-20220802172339-40a2b7c9b9d7
)

require (
	ariga.io/atlas v0.5.0 // indirect
	entgo.io/ent v0.11.1 // indirect
	github.com/agext/levenshtein v1.2.1 // indirect
	github.com/apparentlymart/go-textseg/v13 v13.0.0 // indirect
	github.com/go-openapi/inflect v0.19.0 // indirect
	github.com/go-playground/locales v0.14.0 // indirect
	github.com/go-playground/universal-translator v0.18.0 // indirect
	github.com/go-playground/validator/v10 v10.11.0 // indirect
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/hashicorp/hcl/v2 v2.10.0 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/mitchellh/go-wordwrap v0.0.0-20150314170334-ad45545899c7 // indirect
	github.com/robertkrimen/otto v0.0.0-20211024170158-b87d35c0b86f // indirect
	github.com/shengdoushi/base58 v1.0.0 // indirect
	github.com/zclconf/go-cty v1.8.0 // indirect
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3 // indirect
	golang.org/x/mod v0.5.1 // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/sourcemap.v1 v1.0.5 // indirect
)

replace gitlab.com/pajaziti.bersen/agorus-ent => /Users/sparta/Documents/Projects/agorus-backend/agorus-ent
