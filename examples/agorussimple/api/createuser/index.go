package handler

import (
	"io"
	"context"
	"fmt"
	"log"
	"encoding/json"
	"net/http"

	"gitlab.com/pajaziti.bersen/tine"

	"agorussimple/app"
)

func Createuser(w http.ResponseWriter, r *http.Request) {
	var input map[string]interface{}

	if r.ContentLength != 0 {
		b, err := io.ReadAll(r.Body)

		if err != nil {
			log.Fatal(err)
		}

		err = json.Unmarshal(b, &input)

		if err != nil {
			log.Fatal(err)
		}
	}

	r.Body.Close()

	ctx, err := tine.MakeActionContext(context.Background(), "createuser", tine.Define("input", input))

	App, err := app.MakeAction(ctx, "app")

	if err != nil {
		log.Fatal(err)
	}

	a, _ := App.UseCase(ctx)

	actions, ok := a.(map[string]interface{})

	if !ok {
		log.Fatal("Incorrect Format!")
	}

	ai, ok := actions["createuser"]

	if !ok {
		log.Fatal("Wrong path!")
	}

	action, ok := ai.(tine.Action)

	if !ok {
		log.Fatal("Wrong format!")
	}

	res, err := action.UseCase(ctx)

	if err != nil {
		log.Fatal(err)
	}

	data := res.([]byte)

	fmt.Fprintf(w, string(data))
}
