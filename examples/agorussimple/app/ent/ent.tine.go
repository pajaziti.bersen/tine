package ent

import (
	"context"
	"gitlab.com/pajaziti.bersen/tine"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/pajaziti.bersen/agorus-ent/ent"
	. "gitlab.com/pajaziti.bersen/tine/script"
)



type Factory struct {}

func (f *Factory) UseCase(ctx context.Context) (interface{}, tine.ActionError) {
client, err := ent.Open("sqlite3", "file:ent?mode=memory&cache=shared&_fk=1")
if err != nil {
  return nil, tine.MakeActionError(ctx, err)
}
if err := client.Schema.Create(context.Background()); err != nil {
  return nil, tine.MakeActionError(ctx, err)
}
return client, nil

}

func (f *Factory) Validate(ctx context.Context) tine.ActionError {
	return nil
}


func MakeAction(ctx context.Context, p interface{}) (tine.Action, tine.ActionError) {
	ctx, err := tine.MakeActionContext(ctx, FormatActionName("ent"),
		tine.Define("action", "script"),
		tine.Define("path", "/Users/sparta/Documents/Projects/tine/examples/agorussimple/app/ent"))

	if err != nil {
		return nil, err
	}
	
	return &Factory{}, nil
	
}
