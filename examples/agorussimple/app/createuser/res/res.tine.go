package res

import (
	"context"

	"gitlab.com/pajaziti.bersen/tine"
	"gitlab.com/pajaziti.bersen/tine/action/json"
	. "gitlab.com/pajaziti.bersen/tine/script"
)

type Payload struct {
	Data interface{} `json:"data"`
}

func MakeAction(ctx context.Context, p interface{}) (tine.Action, tine.ActionError) {
	ctx, err := tine.MakeActionContext(ctx, FormatActionName("res"),
		tine.Define("action", "json"),
		tine.Define("path", "/Users/sparta/Documents/Projects/tine/examples/agorussimple/app/createuser/res"))

	if err != nil {
		return nil, err
	}

	Data_i, err := Script(ctx, "user")

	if err != nil {
		return nil, err
	}

	Data := Data_i.(interface{})

	if p != nil {
		json.MakeAction(ctx, p)
	}

	return json.MakeAction(ctx, &Payload{
		Data: Data,
	})
}
