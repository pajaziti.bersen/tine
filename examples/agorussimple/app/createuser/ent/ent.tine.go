package ent

import (
	"context"
	"gitlab.com/pajaziti.bersen/tine"
	"agorussimple/app/ent"
	. "gitlab.com/pajaziti.bersen/tine/script"
)

type Payload struct {
}


func MakeAction(ctx context.Context, p interface{}) (tine.Action, tine.ActionError) {
	ctx, err := tine.MakeActionContext(ctx, FormatActionName("ent"),
		tine.Define("action", "agorussimple/app/ent"),
		tine.Define("path", "/Users/sparta/Documents/Projects/tine/examples/agorussimple/app/createuser/ent"))

	if err != nil {
		return nil, err
	}
	
	if p != nil {
		ent.MakeAction(ctx, p)
	}

	return ent.MakeAction(ctx, &Payload{

	})
}
