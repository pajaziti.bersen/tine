package user

import (
	"agorussimple/app/createuser/userinput"
	"context"

	"gitlab.com/pajaziti.bersen/agorus-ent/ent"
	"gitlab.com/pajaziti.bersen/tine"
	. "gitlab.com/pajaziti.bersen/tine/script"
)

type Factory struct{}

func (f *Factory) UseCase(ctx context.Context) (interface{}, tine.ActionError) {
	c := tine.Variable(ctx, "ent").(*ent.Client)
	i := tine.Variable(ctx, "userinput").(*userinput.Payload)
	u, err := c.User.
		Create().
		SetFullname(i.Name).
		SetHandle("test").
		Save(ctx)

	if err != nil {
		return nil, tine.MakeActionError(ctx, err)
	}
	c.Close()
	return u, nil

}

func (f *Factory) Validate(ctx context.Context) tine.ActionError {
	return nil
}

func MakeAction(ctx context.Context, p interface{}) (tine.Action, tine.ActionError) {
	ctx, err := tine.MakeActionContext(ctx, FormatActionName("user"),
		tine.Define("action", "script"),
		tine.Define("path", "/Users/sparta/Documents/Projects/tine/examples/agorussimple/app/createuser/user"))

	if err != nil {
		return nil, err
	}

	return &Factory{}, nil

}
