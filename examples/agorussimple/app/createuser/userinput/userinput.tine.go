package userinput

import (
	"context"

	"gitlab.com/pajaziti.bersen/tine"
	"gitlab.com/pajaziti.bersen/tine/action/sanatize"
	. "gitlab.com/pajaziti.bersen/tine/script"
)

type Payload struct {
	Name string `json:"name" validate:"alphanum"`
}

func MakeAction(ctx context.Context, p interface{}) (tine.Action, tine.ActionError) {
	ctx, err := tine.MakeActionContext(ctx, FormatActionName("userinput"),
		tine.Define("action", "sanatize"),
		tine.Define("path", "/Users/sparta/Documents/Projects/tine/examples/agorussimple/app/createuser/userinput"))

	if err != nil {
		return nil, err
	}

	Name_i, err := Script(ctx, "input.name")

	if err != nil {
		return nil, err
	}

	Name := Name_i.(string)

	if p != nil {
		sanatize.MakeAction(ctx, p)
	}

	return sanatize.MakeAction(ctx, &Payload{
		Name: Name,
	})
}
