package createuser

import (
	"context"
	"gitlab.com/pajaziti.bersen/tine"
	"agorussimple/app/createuser/userinput"
	"agorussimple/app/createuser/ent"
	"agorussimple/app/createuser/user"
	"agorussimple/app/createuser/res"
	"gitlab.com/pajaziti.bersen/tine/action/process"
	. "gitlab.com/pajaziti.bersen/tine/script"
)

type Payload struct {
	Userinput  func(ctx context.Context) (tine.Action, tine.ActionError)   `json:"userinput" yaml:"userinput"`
	Ent  func(ctx context.Context) (tine.Action, tine.ActionError)   `json:"ent" yaml:"ent"`
	User  func(ctx context.Context) (tine.Action, tine.ActionError)   `json:"user" yaml:"user"`
	Res  func(ctx context.Context) (tine.Action, tine.ActionError)   `json:"res" yaml:"res"`
}


func MakeAction(ctx context.Context, p interface{}) (tine.Action, tine.ActionError) {
	ctx, err := tine.MakeActionContext(ctx, FormatActionName("createuser"),
		tine.Define("action", "process"),
		tine.Define("path", "/Users/sparta/Documents/Projects/tine/examples/agorussimple/app/createuser"))

	if err != nil {
		return nil, err
	}
	
	Userinput := func(ctx context.Context) (tine.Action, tine.ActionError) {
		return userinput.MakeAction(ctx, nil)
  }
	
	Ent := func(ctx context.Context) (tine.Action, tine.ActionError) {
		return ent.MakeAction(ctx, nil)
  }
	
	User := func(ctx context.Context) (tine.Action, tine.ActionError) {
		return user.MakeAction(ctx, nil)
  }
	
	Res := func(ctx context.Context) (tine.Action, tine.ActionError) {
		return res.MakeAction(ctx, nil)
  }
	
    if p != nil {
		process.MakeAction(ctx, p)
	}

	return process.MakeAction(ctx, &Payload{
		Userinput:  Userinput,
		Ent:  Ent,
		User:  User,
		Res:  Res,
	})
}
