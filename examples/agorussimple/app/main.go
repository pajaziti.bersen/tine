package app

import (
	"agorussimple/app/createuser"
	"agorussimple/app/ent"
	"context"

	"gitlab.com/pajaziti.bersen/tine"
	. "gitlab.com/pajaziti.bersen/tine/script"
)

type Payload struct {
	Ent        tine.Action `json:"ent" yaml:"ent"`
	Createuser tine.Action `json:"createuser" yaml:"createuser"`
}

func MakeAction(ctx context.Context, p interface{}) (tine.Action, tine.ActionError) {
	ctx, err := tine.MakeActionContext(ctx, FormatActionName("app"),
		tine.Define("action", "tine"),
		tine.Define("use", "['usecases/createuser.tine']"),
		tine.Define("name", "agorussimple"),
		tine.Define("path", "/Users/sparta/Documents/Projects/tine/examples/agorussimple/app"),
		tine.Define("root", "/Users/sparta/Documents/Projects/tine/examples/agorussimple"),
		tine.Define("project", "agorussimple"))

	if err != nil {
		return nil, err
	}

	Ent, err := ent.MakeAction(ctx, nil)

	if err != nil {
		return nil, err
	}

	Createuser, err := createuser.MakeAction(ctx, nil)

	if err != nil {
		return nil, err
	}

	if p != nil {
		tine.MakeAction(ctx, p)
	}

	return tine.MakeAction(ctx, &Payload{
		Ent:        Ent,
		Createuser: Createuser,
	})
}
