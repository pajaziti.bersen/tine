package requestdata

import (
	"context"
	
	"gitlab.com/pajaziti.bersen/tine"
	"gitlab.com/pajaziti.bersen/tine/action/request"
	. "gitlab.com/pajaziti.bersen/tine/script"
)

type Payload struct {
	Url  string
}


func MakeAction(ctx context.Context, name string) (tine.Action, tine.ActionError) {
	ctx, err := tine.MakeActionContext(ctx, FormatActionName(name),
		tine.Define("action", "request"))

	if err != nil {
		return nil, err
	}
	
	return request.MakeAction(ctx, &Payload{
		Url:  "https://www.gov.uk/bank-holidays.json",
	})
}
