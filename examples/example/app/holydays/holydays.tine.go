package holydays

import (
	"context"
	
	"gitlab.com/pajaziti.bersen/tine"
	"example/app/holydays/requestdata"
	"example/app/holydays/mapdata"
	"example/app/holydays/res"
	"gitlab.com/pajaziti.bersen/tine/action/process"
	. "gitlab.com/pajaziti.bersen/tine/script"
)

type Payload struct {
	Requestdata  func(ctx context.Context) (tine.Action, tine.ActionError) `yaml:"requestdata"`
	Mapdata  func(ctx context.Context) (tine.Action, tine.ActionError) `yaml:"mapdata"`
	Res  func(ctx context.Context) (tine.Action, tine.ActionError) `yaml:"res"`
}


func MakeAction(ctx context.Context, name string) (tine.Action, tine.ActionError) {
	ctx, err := tine.MakeActionContext(ctx, FormatActionName(name),
		tine.Define("action", "process"))

	if err != nil {
		return nil, err
	}
	
	Requestdata := func(ctx context.Context) (tine.Action, tine.ActionError) {
		return requestdata.MakeAction(ctx, "requestdata")
  }
	
	Mapdata := func(ctx context.Context) (tine.Action, tine.ActionError) {
		return mapdata.MakeAction(ctx, "mapdata")
  }
	
	Res := func(ctx context.Context) (tine.Action, tine.ActionError) {
		return res.MakeAction(ctx, "res")
  }
	
	return process.MakeAction(ctx, &Payload{
		Requestdata:  Requestdata,
		Mapdata:  Mapdata,
		Res:  Res,
	})
}
