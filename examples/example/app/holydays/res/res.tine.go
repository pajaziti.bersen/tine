package res

import (
	"context"
	
	"gitlab.com/pajaziti.bersen/tine"
	"gitlab.com/pajaziti.bersen/tine/action/json"
	. "gitlab.com/pajaziti.bersen/tine/script"
)

type Payload struct {
	Data  interface{}
}


func MakeAction(ctx context.Context, name string) (tine.Action, tine.ActionError) {
	ctx, err := tine.MakeActionContext(ctx, FormatActionName(name),
		tine.Define("action", "json"))

	if err != nil {
		return nil, err
	}
	
	Data, err := Script(ctx, "mapdata.title")

	if err != nil {
		return nil, err
	}
	
	return json.MakeAction(ctx, &Payload{
		Data:  Data,
	})
}
