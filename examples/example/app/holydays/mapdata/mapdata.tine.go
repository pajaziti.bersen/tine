package mapdata

import (
	"context"
	
	"gitlab.com/pajaziti.bersen/tine"
	"gitlab.com/pajaziti.bersen/tine/action/jsonpath"
	. "gitlab.com/pajaziti.bersen/tine/script"
)

type Transform struct {
	Title  string
	Date  string
}


func MakeAction(ctx context.Context, name string) (tine.Action, tine.ActionError) {
	ctx, err := tine.MakeActionContext(ctx, FormatActionName(name),
		tine.Define("action", "jsonpath"),
		tine.Define("transform", &Transform{Title: "$.scotland.events[*].title", Date: "$.scotland.events[*].date"}))

	if err != nil {
		return nil, err
	}
	
	Body, err := Script(ctx, "requestdata")

	if err != nil {
		return nil, err
	}
	
	return jsonpath.MakeAction(ctx, Body)
}
