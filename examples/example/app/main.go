package app

import (
	"context"
	
	"gitlab.com/pajaziti.bersen/tine"
	"example/app/settings"
	"example/app/testinput"
	"example/app/holydays"
	. "gitlab.com/pajaziti.bersen/tine/script"
)

type Payload struct {
	Settings  tine.Action `yaml:"settings"`
	Testinput  tine.Action `yaml:"testinput"`
	Holydays  tine.Action `yaml:"holydays"`
}


func MakeAction(ctx context.Context, name string) (tine.Action, tine.ActionError) {
	ctx, err := tine.MakeActionContext(ctx, FormatActionName(name),
		tine.Define("action", "tine"),
		tine.Define("name", "example"),
		tine.Define("path", "/Users/sparta/Documents/Projects/tine/examples/example/app"),
		tine.Define("root", "/Users/sparta/Documents/Projects/tine/examples/example"),
		tine.Define("project", "example"))

	if err != nil {
		return nil, err
	}
	
	Settings, err := settings.MakeAction(ctx, "settings")

	if err != nil {
		return nil, err
	}
	
	Testinput, err := testinput.MakeAction(ctx, "testinput")

	if err != nil {
		return nil, err
	}
	
	Holydays, err := holydays.MakeAction(ctx, "holydays")

	if err != nil {
		return nil, err
	}
	
	return tine.MakeAction(ctx, &Payload{
		Settings:  Settings,
		Testinput:  Testinput,
		Holydays:  Holydays,
	})
}
