package settings

import (
	"context"
	
	"gitlab.com/pajaziti.bersen/tine"
	"gitlab.com/pajaziti.bersen/tine/action/yaml"
	. "gitlab.com/pajaziti.bersen/tine/script"
)

type Payload struct {
	SendMail  bool
	ApiKey  *string `custom:"apiKey"`
	Version  interface{}
}


func MakeAction(ctx context.Context, name string) (tine.Action, tine.ActionError) {
	ctx, err := tine.MakeActionContext(ctx, FormatActionName(name),
		tine.Define("action", "yaml"))

	if err != nil {
		return nil, err
	}
	
	Version, err := Script(ctx, "1 + 2")

	if err != nil {
		return nil, err
	}
	
	return yaml.MakeAction(ctx, &Payload{
		SendMail:  true,
		ApiKey:  OptionalString("lorem"),
		Version:  Version,
	})
}
