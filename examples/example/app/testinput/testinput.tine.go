package testinput

import (
	"context"
	
	"gitlab.com/pajaziti.bersen/tine"
	"gitlab.com/pajaziti.bersen/tine/action/json"
	. "gitlab.com/pajaziti.bersen/tine/script"
)



func MakeAction(ctx context.Context, name string) (tine.Action, tine.ActionError) {
	ctx, err := tine.MakeActionContext(ctx, FormatActionName(name),
		tine.Define("action", "json"))

	if err != nil {
		return nil, err
	}
	
	Body, err := Script(ctx, "input")

	if err != nil {
		return nil, err
	}
	
	return json.MakeAction(ctx, Body)
}
