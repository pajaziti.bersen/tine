package handler

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/pajaziti.bersen/tine"

	"example/app"
)

func Holydays(w http.ResponseWriter, r *http.Request) {

	ctx := context.Background()

	App, err := app.MakeAction(ctx, "app")

	if err != nil {
		log.Fatal(err)
	}

	a, _ := App.UseCase(ctx)

	actions, ok := a.(map[string]interface{})

	if !ok {
		log.Fatal("Incorrect Format!")
	}

	ai, ok := actions["holydays"]

	if !ok {
		log.Fatal("Wrong path!")
	}

	action, ok := ai.(tine.Action)

	if !ok {
		log.Fatal("Wrong format!")
	}

	res, err := action.UseCase(ctx)

	if err != nil {
		log.Fatal(err)
	}

	data := res.([]byte)

	fmt.Fprintf(w, string(data))
}
