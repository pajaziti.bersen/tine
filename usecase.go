package tine

import (
	"context"
	"reflect"
	"strings"
)

func (f *Factory) UseCase(ctx context.Context) (interface{}, ActionError) {
	m := make(map[string]interface{})

	elem := reflect.ValueOf(f.Payload).Elem()

	relType := elem.Type()

	for i := 0; i < relType.NumField(); i++ {
		m[strings.ToLower(relType.Field(i).Name)] = elem.Field(i).Interface()
	}

	return m, nil
}
