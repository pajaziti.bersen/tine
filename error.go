package tine

import "context"

type actionError struct {
	err error
	ctx context.Context
}

func (e *actionError) Error() string {
	return e.err.Error()
}

func (e *actionError) Paths() *map[string]error {
	// TODO: get path from ctx
	return nil
}

func MakeActionError(ctx context.Context, err error) ActionError {
	return &actionError{
		err,
		ctx,
	}
}
