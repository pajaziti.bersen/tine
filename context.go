package tine

import (
	"context"
	"strings"
)

var ctxkey string = "tinecontextkey"

type contextKey string

func (c contextKey) String() string {
	return ctxkey + string(c)
}

type makeContext = func(context.Context) (context.Context, ActionError)

func IsTineKey(ck string) bool {
	return strings.Contains(ck, ctxkey)
}

func GetTineKey(ck string) string {
	return strings.ReplaceAll(ck, ctxkey, "")
}

func Define(key string, value interface{}) makeContext {
	return func(c context.Context) (context.Context, ActionError) {
		return context.WithValue(c, contextKey(key), value), nil
	}
}

func Variable(ctx context.Context, key string) interface{} {
	return ctx.Value(contextKey(key))
}

func StructVariable(ctx context.Context, key string, s interface{}) ActionError {

	return CastStruct(ctx, ctx.Value(contextKey(key)), &s)
}

func SetVariable(ctx context.Context, key string, value interface{}) context.Context {
	return context.WithValue(ctx, contextKey(key), value)
}

func MakeActionContext(ctx context.Context, action string, payload ...makeContext) (context.Context, ActionError) {
	var err ActionError = nil

	makeContexts := []makeContext{Define("parent", ctx), Define("action", action)}

	if len(payload) > 0 {
		makeContexts = append(makeContexts, payload...)
	}

	for _, makeContext := range makeContexts {
		ctx, err = makeContext(ctx)

		if err != nil {
			return ctx, err
		}
	}

	return ctx, err
}
