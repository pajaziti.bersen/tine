package tine

import (
	"context"
)

type (
	Building = *map[string]string

	Payload = map[string]interface{}

	ActionError interface {
		Error() string
		Paths() *map[string]error
	}

	Action interface {
		Validate(ctx context.Context) ActionError
		UseCase(ctx context.Context) (interface{}, ActionError)
	}

	Blueprint interface {
		MakeAction(ctx context.Context, p Payload) (Action, ActionError, context.Context)
	}
)
