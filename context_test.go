package tine

import (
	"context"
	"testing"
)

func TestMakeActionContext(t *testing.T) {
	ctx := context.Background()
	name := "test"

	ctx, err := MakeActionContext(ctx, name)

	if err != nil {
		t.Errorf("Failed! got error")
	}

	if ctx == nil {
		t.Errorf("Failed! got no output")
	}

	v := Variable(ctx, "action")

	if name != v {
		t.Errorf("Failed! expecting %s got %s", name, v)
	}

	t.Logf("Success!")
}
