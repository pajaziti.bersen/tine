package script

import (
	"testing"
)

func TestHash(t *testing.T) {

	d := []byte("This page intentionally left blank.")

	r, err := Hash(d)

	data := string(r)

	t.Logf(string(data))

	if err != nil {
		t.Errorf("Failed! got error %s", err)
		t.FailNow()
	}

	t.Logf("Success!")

}
