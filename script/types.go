package script

import (
	ejson "encoding/json"
)

type Json = ejson.RawMessage

func JsonValue(s interface{}) ejson.RawMessage {
	t, is_string := s.(string)

	if is_string {
		return ejson.RawMessage([]byte(t))
	}

	b := s.([]byte)

	return ejson.RawMessage(b)
}
