package script

func OptionalString(s string) *string {
	r := s

	return &r
}

func OptionalInt64(i int64) *int64 {
	r := i

	return &r
}
