package script

import (
	"crypto/sha256"
	"github.com/shengdoushi/base58"
)

func Hash(bv []byte) ([]byte, error) {
	alf := base58.NewAlphabet("ABCDEFGHJKLMNPQRSTUVWXYZ123456789abcdefghijkmnopqrstuvwxyz")
	hasher := sha256.New()

	hasher.Write(bv)

	s := base58.Encode(hasher.Sum(nil), alf)

	return []byte(s), nil

}
