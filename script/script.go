package script

import (
	"context"
	"errors"
	"fmt"

	"reflect"
	"unsafe"

	"github.com/dop251/goja"
	"gitlab.com/pajaziti.bersen/tine"
)

func setContextToVM(ctx interface{}, vm *goja.Runtime) {
	contextValues := reflect.ValueOf(ctx).Elem()
	contextKeys := reflect.TypeOf(ctx).Elem()

	if contextKeys.Kind() == reflect.Struct {

		var k string

		for i := 0; i < contextValues.NumField(); i++ {
			reflectValue := contextValues.Field(i)
			reflectValue = reflect.NewAt(reflectValue.Type(), unsafe.Pointer(reflectValue.UnsafeAddr())).Elem()

			reflectField := contextKeys.Field(i)

			if reflectField.Name == "Context" {
				setContextToVM(reflectValue.Interface(), vm)
			} else if reflectField.Name == "key" {
				k = fmt.Sprintf("%v", reflectValue.Interface())
			} else if reflectField.Name == "val" && tine.IsTineKey(k) {
				vm.Set(tine.GetTineKey(k), reflectValue.Interface())
			}
		}
	}
}

func Script[T any](ctx context.Context, src string) (*T, tine.ActionError) {

	vm := goja.New()

	vm.SetFieldNameMapper(goja.TagFieldNameMapper("json", true))

	setContextToVM(ctx, vm)

	v, err := vm.RunString(src)

	if err != nil {
		return nil, tine.MakeActionError(ctx, fmt.Errorf("%s; %w", src, err))
	}

	ev := v.Export()

	if ev == nil {
		return nil, nil
	}

	cv, ok := ev.(T)

	if !ok {

		if err == nil {
			return nil, tine.MakeActionError(ctx, errors.New(src))
		}

		return nil, tine.MakeActionError(ctx, err)
	}

	return &cv, nil

}
